package com.smartbox.automation.stepdefs;

import com.smartbox.automation.businessflow.SearchExperience;
import io.cucumber.datatable.DataTable;

import java.util.List;
import java.util.Map;

public class SelectExperienceFromCategoryStepDefs extends BaseStepDefs {
    public SelectExperienceFromCategoryStepDefs(){
        When("I select experience with below category", (DataTable dataTable) -> {
            List<Map<String, String>> dataMap = dataTable.asMaps();
            new SearchExperience().fromCategories(dataMap);
        });

        Then("It should display experience with selected category", () -> {
            new SearchExperience().shouldDisplayRelevantExperiences();
        });

    }
}
