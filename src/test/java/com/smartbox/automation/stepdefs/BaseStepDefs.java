package com.smartbox.automation.stepdefs;

import com.smartbox.automation.utils.ContextManager;
import cucumber.api.java8.En;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseStepDefs implements En {
    private static Logger LOGGER = LogManager.getLogger(BaseStepDefs.class);
    protected ContextManager contextManager = ContextManager.getInstance();

    public BaseStepDefs() {
        LOGGER.debug("Instantiated " + BaseStepDefs.class.getName());
    }
}
