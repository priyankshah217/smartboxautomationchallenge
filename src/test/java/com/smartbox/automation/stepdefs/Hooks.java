package com.smartbox.automation.stepdefs;


import com.smartbox.automation.utils.ContextManager;
import com.smartbox.automation.utils.DriverFactory;
import com.smartbox.automation.utils.KEYS;
import com.smartbox.automation.utils.ScreenShotUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.monte.media.Format;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.WebDriver;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.monte.media.FormatKeys.*;
import static org.monte.media.VideoFormatKeys.*;


public class Hooks {
    private ContextManager contextManager = ContextManager.getInstance();
    private static Logger LOGGER = LogManager.getLogger(Hooks.class);
    private ScreenRecorder screenRecorder;

    private void loadConfigPropertiesToContext() {
        try {
            LOGGER.debug("Loading automation properties");
            Properties properties = new Properties();
            String projectDir = System.getProperty("user.dir");
            FileInputStream inputStream = new FileInputStream(new File(projectDir + "/src/test/resources/AutomationConfig.properties"));
            properties.load(inputStream);
            contextManager.addToContext(KEYS.BROWSER_TYPE, System.getProperty("browser.type"));
            contextManager.addToContext(KEYS.MODE, System.getProperty("mode"));
            contextManager.addToContext(KEYS.GECKO_DRIVER_PATH, properties.getProperty("firefox.driver.path"));
            contextManager.addToContext(KEYS.CHROME_DRIVER_PATH, properties.getProperty("chrome.driver.path"));
            contextManager.addToContext(KEYS.APPLICATION_URL, properties.getProperty("application.url"));
            contextManager.addToContext(KEYS.VIDEO_CAPTURE_ENABLE, properties.getProperty("video.capture"));
            contextManager.addToContext(KEYS.VIDEO_FORMAT, properties.getProperty("video.format"));
        } catch (IOException e) {
            LOGGER.debug("Properties file is  missing");
            e.printStackTrace();
        }
    }

    private void launchApplication(String appUrl) {
        DriverFactory driverFactory = new DriverFactory();
        try {
            contextManager.addToContext(KEYS.DRIVER, driverFactory.getDriver());
        } catch (Exception e) {
            e.printStackTrace();
        }
        contextManager.driver().get(appUrl);
    }

    @Before
    public void init(Scenario scenario) throws Exception {
        loadConfigPropertiesToContext();
        String scenarioStartMessage = "\n" +
                "\n--------------------------------------------------------------------------------------------\n" +
                "\tScenario: '" + scenario.getName() + "\n" +
                "\n--------------------------------------------------------------------------------------------\n";
        LOGGER.debug(scenarioStartMessage);
        launchApplication(contextManager.get(KEYS.APPLICATION_URL).toString());
        if (Boolean.parseBoolean(contextManager.get(KEYS.VIDEO_CAPTURE_ENABLE).toString()))
            startRecording(scenario);
    }

    public void startRecording(Scenario scenario) throws Exception {
        String reportDir = new File(System.getenv("reportsDir")).getAbsolutePath();
        String outputFileName = scenario.getName().replace(" ", "");
        String videoName = reportDir + "/" + outputFileName;
        GraphicsConfiguration graphicsConfiguration = GraphicsEnvironment
                .getLocalGraphicsEnvironment()
                .getDefaultScreenDevice()
                .getDefaultConfiguration();
        screenRecorder = new ScreenRecorder(graphicsConfiguration,
                graphicsConfiguration.getBounds(),
                new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI),
                new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
                        CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
                        DepthKey, 24, FrameRateKey, Rational.valueOf(15),
                        QualityKey, 1.0f,
                        KeyFrameIntervalKey, 15 * 60),
                new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, "black", FrameRateKey, Rational.valueOf(30)),
                null,
                new File(videoName));
        screenRecorder.start();
    }

    public void stopRecording() throws Exception {
        screenRecorder.stop();
    }

    @After
    public void tearDown(Scenario scenario) throws Exception {
        String scenarioEndMessage = "\n" +
                "\n--------------------------------------------------------------------------------------------\n" +
                "\tExecuted scenario  " + scenario.getName() + "\n " +
                "\tStatus : " + scenario.getStatus() + "\n" +
                "\n--------------------------------------------------------------------------------------------\n";
        LOGGER.debug(scenarioEndMessage);
        ScreenShotUtils.embedScreenShotInReport(scenario);
        if (Boolean.parseBoolean(contextManager.get(KEYS.VIDEO_CAPTURE_ENABLE).toString())) {
            stopRecording();
            ScreenShotUtils.embedVideoInReport(scenario);
        }
        ((WebDriver) contextManager.get(KEYS.DRIVER)).quit();
    }
}
