package com.smartbox.automation.stepdefs;

import com.smartbox.automation.businessflow.AddToCartExperience;
import com.smartbox.automation.businessflow.SearchExperience;
import com.smartbox.automation.businessflow.SelectExperience;
import io.cucumber.datatable.DataTable;

import java.util.List;
import java.util.Map;

public class AddToCartStepDefs extends BaseStepDefs {

    public AddToCartStepDefs() {
        When("I create memory with below data", (DataTable dataTable) -> {
            List<Map<String, String>> dataMap = dataTable.asMaps();
            new SearchExperience().createExperience(dataMap);
        });

        Then("It should display experience as per selected criteria", () -> {
            new SelectExperience().hasExperienceCreated();
        });

        When("I select one of experience", () -> {
            new AddToCartExperience().selectExperienceAndAddToCart();
        });

        When("I add it to cart", () -> {
            new AddToCartExperience().addToCartExperience();
        });

        Then("Experience should be added to cart", () -> {
            new AddToCartExperience().experienceShouldBeAddedToCart();
        });
    }
}
