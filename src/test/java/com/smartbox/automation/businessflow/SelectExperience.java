package com.smartbox.automation.businessflow;

import com.smartbox.automation.screens.ListOfExperienceScreen;

import static com.smartbox.automation.utils.CustomAsserts.assertThat;

public class SelectExperience extends BaseBusinessFlow {

    public void hasExperienceCreated() {
        assertThat("Experience is not created", new ListOfExperienceScreen().checkListOfExperiences());
    }
}
