package com.smartbox.automation.businessflow;

import com.smartbox.automation.utils.ContextManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseBusinessFlow {
    protected static Logger LOGGER = LogManager.getLogger(BaseBusinessFlow.class);
    protected ContextManager contextManager = ContextManager.getInstance();
}
