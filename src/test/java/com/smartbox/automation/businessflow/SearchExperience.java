package com.smartbox.automation.businessflow;

import com.smartbox.automation.screens.HomeScreen;
import com.smartbox.automation.screens.ListOfExperienceScreen;
import com.smartbox.automation.utils.KEYS;

import java.util.List;
import java.util.Map;

import static com.smartbox.automation.utils.CustomAsserts.assertThat;

public class SearchExperience extends BaseBusinessFlow {
    public void createExperience(List<Map<String, String>> dataMap) {
        new HomeScreen().createExperienceWithOptions(dataMap);
    }

    public void fromCategories(List<Map<String, String>> dataMap) {
        String category = dataMap.get(0).get("category");
        String subCategory = dataMap.get(0).get("subcategory");
        contextManager.addToContext(KEYS.PRODUCTCATEGORY, category);
        contextManager.addToContext(KEYS.PRODUCTSUBCATEGORY, subCategory);
        new HomeScreen().searchExperienceFromCategory(category, subCategory);
    }

    public void shouldDisplayRelevantExperiences() {
        assertThat("Selected experience is not shown",
                new ListOfExperienceScreen().hasExperienceWithCategory(contextManager.get(KEYS.PRODUCTSUBCATEGORY).toString()));
    }
}
