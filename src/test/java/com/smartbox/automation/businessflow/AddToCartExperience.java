package com.smartbox.automation.businessflow;


import com.smartbox.automation.screens.CheckoutScreen;
import com.smartbox.automation.screens.ExperienceDetailScreen;
import com.smartbox.automation.screens.ListOfExperienceScreen;

import static com.smartbox.automation.utils.CustomAsserts.assertThat;

public class AddToCartExperience extends BaseBusinessFlow {

    public void selectExperienceAndAddToCart() {
        new ListOfExperienceScreen().selectFirstExperience();
    }


    public void addToCartExperience() {
        new ExperienceDetailScreen().addToCartExperience();
    }

    public void experienceShouldBeAddedToCart() {
        assertThat("Experience is added to cart",new CheckoutScreen().isExperienceAddedToCart());
    }
}
