package com.smartbox.automation.screens;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.Map;

public class HomeScreen extends AbstractScreen {
    private static Logger LOGGER = LogManager.getLogger(HomeScreen.class);
    private String cookieDialogLocator = "optanon-popup-bg";
    private String acceptCookieButtonLocator = "a.optanon-allow-all.accept-cookies-button";
    private String budgetRadioButtonLocator = "a.btn.btn-primary.btn-sm.notActive.btn-budget > span";
    private String selectPersonLocator = "button.people-selector__btn.people-selector__btn--plus";
    private String experienceDropdownLocator = "universe[]";
    private String submitButtonLocator = "button.button.primary";

    public HomeScreen() {
        super();
        LOGGER.debug("Instantiated " + HomeScreen.class.getName());
    }

    public void createExperienceWithOptions(List<Map<String, String>> dataMap) {
        dismissCookiePopup();
        scrollUp();
        int noOfPerson = Integer.parseInt(dataMap.get(0).get("person"));
        for (int selectperson = 0; selectperson < noOfPerson; selectperson++) {
            clickOnElement(By.cssSelector(selectPersonLocator));
        }
        clickOnElement(By.id(experienceDropdownLocator));
        selectFromList(By.id(experienceDropdownLocator), dataMap.get(0).get("experience"));
        clickOnElement(By.id(experienceDropdownLocator));
        WebElement budgetSelectButton = getWebElementFromList(dataMap.get(0).get("budget"),
                By.cssSelector(budgetRadioButtonLocator));
        clickOnElement(budgetSelectButton);
        if (driver instanceof ChromeDriver && waitForElementToBeVisible(By.cssSelector("iframe.fpw-view"), 10)) {
            clickOnElement(driver.switchTo().frame(4).findElement(By.xpath("//p/span")));
            driver.switchTo().defaultContent();
        }
        clickOnElement(By.cssSelector(submitButtonLocator));
    }


    private void dismissCookiePopup() {
        if (isElementPresent(By.id(cookieDialogLocator))) {
            if (waitForElementToBeVisible(By.cssSelector(acceptCookieButtonLocator))) {
                pressKeyStrokes(By.cssSelector(acceptCookieButtonLocator), Keys.TAB);
                pressKeyStrokes(By.cssSelector(acceptCookieButtonLocator), Keys.ENTER);
            }
        }
    }


    public void searchExperienceFromCategory(String category, String subCategory) {
        dismissCookiePopup();
        scrollUp();
        WebElement mainMenu = getWebElement(By.linkText(category));
        Actions actions = new Actions(driver);
        actions.moveToElement(mainMenu).build().perform();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", getWebElement(By.linkText(subCategory)));
    }
}
