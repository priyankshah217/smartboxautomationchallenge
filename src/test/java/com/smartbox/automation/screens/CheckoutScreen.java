package com.smartbox.automation.screens;

import org.openqa.selenium.By;

public class CheckoutScreen extends AbstractScreen {
    private String addToCartProductCountLocator = "//section[@id='header-dropdown-wrapper']/div[2]/a/span/span";

    public boolean isExperienceAddedToCart() {
        return getWebElement(By.xpath(addToCartProductCountLocator)).getText().equals("1");
    }
}
