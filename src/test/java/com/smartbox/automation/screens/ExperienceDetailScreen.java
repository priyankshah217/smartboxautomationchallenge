package com.smartbox.automation.screens;

import org.openqa.selenium.By;

public class ExperienceDetailScreen extends AbstractScreen {
    private String addToCartLocator = "span.add-to-cart__content";
    private String addToCartContinueButton = "a.button.primary.no-margin-bottom";


    public void addToCartExperience() {
        clickOnElement(By.cssSelector(addToCartLocator));
        waitUntilElementDisappear(By.cssSelector(addToCartLocator), 2);
        clickOnElement(By.cssSelector(addToCartContinueButton));
    }
}
