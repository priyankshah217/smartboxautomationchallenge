package com.smartbox.automation.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ListOfExperienceScreen extends AbstractScreen {
    private String listOfExperienceThumbnailLocator = "ac-cloudSearchResults";
    private String experienceTitleText = "h1.description-block__title";

    public boolean checkListOfExperiences() {
        scrollUp();
        return getFirstWebElementFromList(By.id(listOfExperienceThumbnailLocator)) != null;
    }

    public void selectFirstExperience() {
        WebElement webElement = getFirstWebElementFromList(By.id(listOfExperienceThumbnailLocator));
        clickOnElement(webElement.findElement(By.tagName("img")));
    }

    public boolean hasExperienceWithCategory(String experienceName) {
        waitForElementToBeVisible(By.cssSelector(experienceTitleText));
        String actualText = getWebElement(By.cssSelector(experienceTitleText)).getText().trim().toLowerCase();
        LOGGER.debug("Actual text is {}", actualText);
        LOGGER.debug("Expected text is {}", experienceName.toLowerCase());
        return actualText.contains(experienceName.toLowerCase());
    }
}
