package com.smartbox.automation.screens;

import com.smartbox.automation.utils.ContextManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public abstract class AbstractScreen {

    protected static Logger LOGGER = LogManager.getLogger(AbstractScreen.class);
    protected ContextManager contextManager = ContextManager.getInstance();
    WebDriver driver;

    protected AbstractScreen() {
        driver = contextManager.driver();
    }

    protected boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            LOGGER.debug("Element is present {}", by.toString());
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    boolean waitForElementToBeVisible(By by) {
        return waitForElementToBeVisible(by, 30);
    }

    boolean waitForElementToBeVisible(By by, int timeout) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            return wait.until((ExpectedCondition<Boolean>) driver -> isElementPresent(by));
        } catch (TimeoutException ex) {
            LOGGER.debug("Element not found " + by.toString());
            return false;
        }
    }

    protected void clickOnElement(By by, int timeout) {
        getWebElement(by, timeout).click();
    }

    protected void clickOnElement(By by) {
        getWebElement(by, 30).click();
    }

    protected void clickOnElement(WebElement webElement) {
        clickOnElement(webElement, 30);
    }

    private void clickOnElement(WebElement webElement, int timeout) {
        try {
            if (waitForElementToBeVisible(webElement, timeout)) {
                webElement.click();
            }
            LOGGER.debug("clicked on " + webElement.toString());
        } catch (Exception ex) {
//            LOGGER.debug("Page Source \n {}", driver.getPageSource());
        }
    }

    protected void enterTextTo(By by, String textValue) {
        enterTextTo(by, textValue, 30);
    }

    private void enterTextTo(By by, String textValue, int timeout) {
        WebElement webElement;
        webElement = getWebElement(by);
        enterTextTo(webElement, textValue);
        LOGGER.debug("Entered " + textValue + " in  " + by.toString());
    }

    void enterTextTo(WebElement webElement, String textValue) {
        enterTextTo(webElement, textValue, 30);
    }

    private void enterTextTo(WebElement webElement, String textValue, int timeout) {
        try {
            if (waitForElementToBeVisible(webElement, timeout)) {
                try {
                    webElement.clear();
                } catch (InvalidElementStateException ex) {
                    LOGGER.debug(ex.getMessage());
                }
                webElement.sendKeys(textValue);
            }
            LOGGER.debug("Entered " + textValue + " in  " + webElement.toString());
        } catch (Exception ex) {
//            LOGGER.debug("Page Source \n {}", driver.getPageSource());
        }
    }

    private boolean waitForElementToBeVisible(WebElement webElement, int timeout) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.elementToBeClickable(webElement));
            LOGGER.debug("Element is clickable {}", webElement.toString());
            return true;
        } catch (TimeoutException | NoSuchElementException ex) {
            LOGGER.debug("Element not visible " + webElement.toString());
            return false;
        }
    }

    void pressKeyStrokes(By by, Keys keys) {
        if (waitForElementToBeVisible(by, 30)) {
            WebElement webElement = driver.findElement(by);
            webElement.sendKeys(keys);
        }
    }

    protected void scrollUp() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, -document.body.scrollHeight)");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected WebElement getWebElementFromList(String budgetText, By by) {
        List<WebElement> webElementList = getListOfWebElements(by);
        return webElementList.stream().filter(webElement -> webElement.getText().equalsIgnoreCase(budgetText)).findFirst().get();
    }

    protected WebElement getFirstWebElementFromList(By by) {
        List<WebElement> webElementList = getListOfWebElements(by);
        return webElementList.stream().findFirst().get();
    }

    protected void selectFromList(By by, String optionText) {
        new Select(getWebElement(by)).selectByVisibleText(optionText);
    }

    protected WebElement getWebElement(By by) {
        return getWebElement(by, 30);
    }

    protected List<WebElement> getListOfWebElements(By by) {
        return getListOfWebElements(by, 30);
    }

    protected WebElement getWebElement(By by, int timeout) {
        if (waitForElementToBeVisible(by, timeout))
            return driver.findElement(by);
        else
            return null;
    }

    protected List<WebElement> getListOfWebElements(By by, int timeout) {
        if (waitForElementToBeVisible(by, timeout))
            return driver.findElements(by);
        else
            return null;
    }

    boolean waitUntilElementDisappear(By by) {
        return waitUntilElementDisappear(by, 30);
    }

    boolean waitUntilElementDisappear(By by, int timeout) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            return wait.until((ExpectedCondition<Boolean>) driver -> !isElementPresent(by));
        } catch (TimeoutException ex) {
            LOGGER.debug("Element not found " + by.toString());
            return false;
        }
    }
}
