package com.smartbox.automation.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.util.concurrent.TimeUnit;


public class DriverFactory {

    private static Logger LOGGER = LogManager.getLogger(DriverFactory.class);

    private ContextManager contextManager = ContextManager.getInstance();

    public WebDriver getDriver() throws Exception {
        WebDriver webDriver;
        String browserType = contextManager.get(KEYS.BROWSER_TYPE).toString();
        String mode = contextManager.get(KEYS.MODE).toString();
        if (mode.equalsIgnoreCase("local")) {
            switch (browserType) {
                case BrowserType.FIREFOX:
                    System.setProperty("webdriver.gecko.driver", contextManager.get(KEYS.GECKO_DRIVER_PATH).toString());
                    webDriver = new FirefoxDriver();
                    break;
                case BrowserType.CHROME:
                    System.setProperty("webdriver.chrome.driver", contextManager.get(KEYS.CHROME_DRIVER_PATH).toString());
                    webDriver = new ChromeDriver();
                    break;
                default:
                    throw new IllegalArgumentException("Invalid browser type set in class injection " + browserType);
            }
            initWebDriver(webDriver);
            return webDriver;
        } else {
            switch (browserType) {
                case BrowserType.FIREFOX:
                    webDriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), new FirefoxOptions());
                    break;
                case BrowserType.CHROME:
                    webDriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), new ChromeOptions());
                    break;
                default:
                    throw new IllegalArgumentException("Invalid browser type set in class injection " + browserType);
            }
            initWebDriver(webDriver);
            return webDriver;
        }
    }

    private void initWebDriver(WebDriver webDriver) {
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
    }

}
