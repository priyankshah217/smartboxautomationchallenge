package com.smartbox.automation.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

class CommandLineUtils {
    private static Logger LOGGER = LogManager.getLogger(CommandLineUtils.class);

    static void runCommand(String commandName) {
        ProcessBuilder processBuilder = new ProcessBuilder("/bin/sh", "-c", commandName);
        try {
            LOGGER.debug("Running command " + commandName);
            processBuilder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
