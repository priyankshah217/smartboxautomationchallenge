package com.smartbox.automation.utils;

import cucumber.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ScreenShotUtils {
    private static Logger LOGGER = LogManager.getLogger(ScreenShotUtils.class);

    private static ContextManager contextManager = ContextManager.getInstance();

    private static int screenShotCounter;

    private ScreenShotUtils() {
        reset();
    }

    static String saveScreenShotAs(String fileName, String comment) {
        return saveScreenShotAndPageSourceAs(fileName, comment, null);
    }

    private static String saveScreenShotAndPageSourceAs(String fileName, String comment, String pageSource) {
        WebDriver driver = contextManager.driver();
        String reportDir = new File(System.getenv("reportsDir")).getAbsolutePath();
        String savedFileName = "";
        if (null != driver) {
            String counter = getScreenShotCounter();
            try {
                FileUtils.forceMkdir(new File(reportDir + "/screenshots"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            savedFileName = reportDir + "/screenshots/" + counter + "-" + comment.replace(" ", "");
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            screenshot.renameTo(new File(savedFileName + ".jpg"));
            LOGGER.debug("\n\t" + comment + "\n\t\tScreen shot saved in: " + savedFileName + ".jpg");
        }
        return savedFileName;
    }

    public static void embedScreenShotInReport(Scenario scenario) {
        LOGGER.debug("Finished running scenario - '" + scenario.getName() + "', Embedding screenshot in report");
        WebDriver driver = contextManager.driver();
        if (null != driver) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
        }
    }

    private static String getScreenShotCounter() {
        return String.format("%03d", ++screenShotCounter);
    }

    static void reset() {
        LOGGER.debug("Setting ScreenShot Counter to 0");
        screenShotCounter = 0;
    }

    public static void startVideoRecording() {
        String startVideoCommand = "flick video -a start -p android -u " + contextManager.get(KEYS.DEVICE_ID) + " -c 1000";
        CommandLineUtils.runCommand(startVideoCommand);
    }

    public static void stopVideoRecording(String reportDir, String outputFileName, String videoFormat) {
        String stopVideoCommand =
                "flick video -a stop -p android -r 5 -e -u " + contextManager.get(KEYS.DEVICE_ID) + " -o " + reportDir +
                        " -n " + outputFileName + " -f " + videoFormat;
        CommandLineUtils.runCommand(stopVideoCommand);
    }

    public static void embedVideoInReport(Scenario scenario) {
        String reportDir = new File(System.getenv("reportsDir")).getAbsolutePath();
        String outputFileName = scenario.getName().replace(" ", "");
        String videoFormat = (String) contextManager.get(KEYS.VIDEO_FORMAT);
        stopVideoRecording(reportDir, outputFileName, videoFormat);
        String videoName = reportDir + "/" + outputFileName + "." + videoFormat;
        String mime = videoFormat.equalsIgnoreCase("avi") ? "video" : "image";
        LOGGER.debug("Video captured at " + videoName);
        String embedVideo =
                "<video width=\"320\" height=\"240\" controls>\n" +
                        "  <source src=\"" + videoName + "\" type=\"" + mime + "/" + videoFormat + "\">\n" +
                        "</video>";
        scenario.embed(embedVideo.getBytes(StandardCharsets.UTF_8), "text/html");
    }
}
