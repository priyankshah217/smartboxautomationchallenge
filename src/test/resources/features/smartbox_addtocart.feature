Feature: Add experience to cart
  As a Customer
  I want to find a desired box and add it to the cart
  So I can make adjustments prior to checkout

  @AddToCart @e2e
  Scenario: Add to cart selected experience
    When I create memory with below data
      | person | experience | budget        |
      | 5      | Séjour     | De 50 à 100 € |
    Then It should display experience as per selected criteria
    When I select one of experience
    And I add it to cart
    Then Experience should be added to cart