Feature: Select Experience
  As a customer
  I want to view a list of products
  So I can select some to purchase

  @SelectExperience @e2e
  Scenario: Select experience from different category
    When I select experience with below category
      | category    | subcategory |
      | Gastronomie | Bistrots    |
    Then It should display experience with selected category