### Libraries and Plugin Used:
1. Hamcrest asserts (http://hamcrest.org/JavaHamcrest/)
2. Cucumber-jvm (https://cucumber.io/docs/reference/jvm#java)
3. Cucumber gradle parallel (https://github.com/camiloribeiro/cucumber-gradle-parallel)

### Pre-requisite:
1. Copy browser specific drivers to `drivers`
2. Provide below parameters to `AutomaitonConfig.properties`
    
    `video.capture=true/false`
    
    `video.format=avi`
3. (Optional) Install docker & docker-compose -- For headless run
4. Download and invoke Zalenium grid images https://github.com/zalando/zalenium#run-it -- Try it really cool !!!
     
### Components:
- Groovy scripts:
    1. TaskExecutor: Driver script for cucumber (run parallel and sequential)
    2. Utils: Implementations of miscellaneous functions
    3. Reporter: Generate cucumber report
- Step definitions: Contains implementations of cucumber steps, interacts with business flows 
- Business flows: Additional layers which can call other business flows or screen methods
- Screens: Implementation of pages for mobile apps
- Have descriptive logging using log4j
- Utilities:
    1. Custom Asserts: Extension of hemcrest assert, which provide crisp assert messages
    2. DriverFactory: Gives driver objects based on gradle task
    3. KEYS: An enum for relevant properties
    4. ScreenShotUtils: Captures screenshots of browsers, Also captures video recording and attached to reports

### Run:
`run=@AddToCart/@e2e/@SelectExperience ./gradlew clean runCukes -Dmode=<local/remote> -Dbrowser.type=<firefox/chrome>`

**Note:** In order to run test inside headless mode (Container), below command has to be invoke

`docker-compose up -d` or invoke zalenium (Using its live dashboard you can see parallel run)

`run=@AddToCart/@e2e/@SelectExperience ./gradlew clean runCukes -Dmode=remote -Dbrowser.type=<firefox/chrome>`

### Reports:
1. After a run reports will be generated to /testRun/report_MMddyyhhmmss/cucumer/cucumber-html-reports/overview-features.html
2. Last screenshot of scenario also attached to report

### Reports Images:
![](images/SummaryReport.png)

![](images/DetailReport.png)


### Scope for further extensions:
1. Parallel or Distributed execution at feature and scenario levels

### References:

1. Organize logic using build src 
https://docs.gradle.org/current/userguide/organizing_build_logic.html#sec:build_sources

2. Run cucumber parallel
https://github.com/camiloribeiro/cucumber-gradle-parallel

3. Cucumber and Infra
https://github.com/anandbagmar/cucumber-jvm-appium-infra
 