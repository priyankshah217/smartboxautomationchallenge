package com.smartbox.buildscripts

import groovy.io.FileType
import net.masterthought.cucumber.Configuration
import net.masterthought.cucumber.ReportBuilder

class Reporter {

    private static List<String> getListOfJsonReports(File reportOutputDirectory) {
        List<String> jsonReportFiles = new ArrayList()

        reportOutputDirectory.eachFileRecurse(FileType.FILES) { file ->
            if (file.name.endsWith(".json")) {
                jsonReportFiles.add(file.getAbsolutePath())
            }
        }
        jsonReportFiles
    }

    static def generateReport(String reportsDir) {
        File reportOutputDirectory = new File(reportsDir + "/cucumber")
        List<String> jsonReportFiles = getListOfJsonReports(reportOutputDirectory)
        Configuration configuration = new Configuration(reportOutputDirectory, "TestAutomation")
        // optional configuration
        configuration.setRunWithJenkins(false)
        ReportBuilder reportBuilder = new ReportBuilder(jsonReportFiles, configuration)
        reportBuilder.generateReports()
        println("\nReport available on: " + reportsDir + "/cucumber/cucumber-html-reports/overview-features.html")
    }
}
