package com.smartbox.buildscripts

import java.text.SimpleDateFormat


class Utils {
    static project = null
    static def runShellCommand(String command, String message, Boolean printOutput = true) {
        println "[SHELL COMMAND] Started: " + new Date()
        println "[SHELL COMMAND] $message - '${command}'"
        def stdoutFromCommand = new ByteArrayOutputStream()

        project.exec {
            commandLine 'sh', "-c", command
            standardOutput = stdoutFromCommand
        }
        def commandOutput = stdoutFromCommand.toString()
        if (printOutput) {
            println commandOutput
        }
        println "[SHELL COMMAND] Finished: " + new Date()
        commandOutput
    }


    static String getReportsDir() {
        def date = new Date()
        def sdf = new SimpleDateFormat("MMddyyyyhhmmss")
        def reportsDirectory = 'testRun/reports_' + sdf.format(date)
        createDirectory(reportsDirectory)
        return reportsDirectory
    }

    static def createDirectory(String reportsDirectory) {
        File f = new File(reportsDirectory + "/logs")
        if (!f.exists()) {
            println "Creating directory - $reportsDirectory/logs"
            f.mkdirs()
        }
    }


    static def sleepFor(int seconds) {
        println "Waiting for $seconds seconds"
        sleep(seconds * 1000)
    }
}