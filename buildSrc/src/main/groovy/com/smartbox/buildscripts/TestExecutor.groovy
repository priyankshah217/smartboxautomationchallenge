package com.smartbox.buildscripts

import groovy.io.FileType
import groovyx.gpars.GParsPool

class TestExecutor {

    static
    def runTest(String reportsDir, String featureFileDir) {
        def envRunTags = System.getenv("run")
        def arglist = ["-p", "pretty", "-p", "json:" + reportsDir + "/cucumber/cucumber.json", "--glue", "com" +
                ".smartbox" +
                ".automation",
                       featureFileDir, "-t", envRunTags]
        def log4jConfigPath = "src/test/resources/log4j2.properties"
        def runStatus = Utils.project.javaexec {
            ignoreExitValue = true
            main = "cucumber.api.cli.Main"
            classpath = Utils.project.configurations.cucumberRuntime + Utils.project.sourceSets.test.output
            environment "reportsDir", reportsDir
            systemProperties System.getProperties()
            args = arglist
            jvmArgs = ["-Dlog4j.configurationFile=" + log4jConfigPath]
        }
        if (runStatus.getExitValue() != 0) {
            Utils.project.ext.set("exitStatus", runStatus)
        }
        runStatus
    }

    static
    def runCucumberTestsParallel(reportsDir, featureFilesPath) {
        List<File> featureList = new ArrayList()
        File featureFileDir = new File("${featureFilesPath}")
        featureFileDir.eachFileRecurse(FileType.FILES) { file ->
            if (file.name.endsWith(".feature")) {
                featureList.add(new File(file.getAbsolutePath()))
            }
        }
        def poolSize = featureList.size()
        GParsPool.withPool(poolSize) {
            featureList.eachParallel { File file ->
                def envRunTags = System.getenv("run")
                def arglist = ["-p", "pretty", "-p", "json:" + reportsDir + "/cucumber/cucumber_" + file.name + ".json",
                               "--glue",
                               "com.smartbox.automation",
                               file.getAbsolutePath(), "-t", envRunTags]
                def log4jConfigPath = "src/test/resources/log4j2.properties"
                def runStatus = Utils.project.javaexec {
                    ignoreExitValue = true
                    main = "cucumber.api.cli.Main"
                    classpath = Utils.project.configurations.cucumberRuntime + Utils.project.sourceSets.test.output
                    environment "reportsDir", reportsDir
                    systemProperties System.getProperties()
                    args = arglist
                    jvmArgs = ["-Dlog4j.configurationFile=" + log4jConfigPath]
                }
                if (runStatus.getExitValue() != 0) {
                    Utils.project.ext.set("exitStatus", runStatus)
                }
            }
        }
    }
}
